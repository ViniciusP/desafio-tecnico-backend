import express from 'express';
import CardValidator from '../validator';
import Middleware from '../../middleware';
import CardController from '../controller';

const router = express.Router();

router.post(
	'/',
	CardValidator.checkCreateCard(),
	Middleware.handleValidationError,
	CardController.create
);

router.get(
	'/',
    [],
	Middleware.handleValidationError,
	CardController.list
);

router.put(
	'/:id',
	CardValidator.checkIdParam(),
	Middleware.handleValidationError,
	CardController.update
);

router.delete(
	'/:id',
	CardValidator.checkIdParam(),
	Middleware.handleValidationError,
	CardController.delete
);

export default router;
