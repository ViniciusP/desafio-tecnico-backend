import { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import { CardInstance } from "../model";

class CardController {
	async create(req: Request, res: Response) {
		const id = uuidv4();
		try {
			const record = await CardInstance.create({ ...req.body, id });
			return res.json({ record, msg: "Successfully create card" });
		} catch (e) {
			return res.json({ msg: "fail to create", status: 500, route: "/create" });
		}
	}

	async list(_req: Request, res: Response) {
		try {
			const records = await CardInstance.findAll({});
			return res.json(records);
		} catch (e) {
			return res.json({ msg: "fail to read", status: 500, route: "/read" });
		}
	}

	async update(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const record = await CardInstance.findOne({ where: { id } });

			if (!record) {
				return res.json({ msg: "Can not find existing record" });
			}

			const updatedRecord = await record.update({ ...req.body });
			return res.json({ record: updatedRecord });
		} catch (e) {
			return res.json({
				msg: "fail to read",
				status: 500,
				route: "/update/:id",
			});
		}
	}
	async delete(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const record = await CardInstance.findOne({ where: { id } });

			if (!record) {
				return res.json({ msg: "Can not find existing record" });
			}

			const deletedRecord = await record.destroy();
			return res.json({ record: deletedRecord });
		} catch (e) {
			return res.json({
				msg: "fail to read",
				status: 400,
				route: "/delete/:id",
			});
		}
	}
}

export default new CardController();
