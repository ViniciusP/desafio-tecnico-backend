import { DataTypes, Model } from 'sequelize';
import db from '../../config/database.config';

interface CardAttributes {
	id: string;
	titulo: string;
	conteudo: string;
    lista: string;
}

export class CardInstance extends Model<CardAttributes> {}
CardInstance.init(
	{
		id: {
			type: DataTypes.UUIDV4,
			primaryKey: true,
			allowNull: false,
		},
		titulo: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		conteudo: {
			type: DataTypes.STRING,
			allowNull: false,
		},
        lista: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		sequelize: db,
		tableName: 'cards',
	}
);
