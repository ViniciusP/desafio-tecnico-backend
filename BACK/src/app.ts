import express from "express";
import cartRouter from "./card/route";

const app = express();
app.use(express.json());
app.use("/cards", cartRouter);

export default app;